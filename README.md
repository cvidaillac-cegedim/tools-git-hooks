# tools-git-hooks

## Introduction
This repository is used to store git templates (such as hooks mainly).

There are many types of hooks, we focus here on the pre-commit hook.

the pre-commit hook will try to run the spotlessApply task (on beyond apps only) through `./gradlew spotlessApply` command.

If you encounter any issue you can bypass this hook with `git commit --no-verify`

## Installation

This installation **will only affect newly created repo** (init or cloned ones).

After init or clone, in the .git/hooks/ directory of your repository you will see your pre-commit script.

This script will be automatically triggered by Git when on your next commit.

### Windows
In git bash run `./add-git-hooks.sh` script.

### Linux
Simply run add-git-hooks.sh

